
The original authors of this code are Louis Duval (louis.duval@ens-paris-saclay.fr) and Lucas Feron (lucas.feron97@gmail.com).

A description of the functions they have implemented can be found in the folder "resources" in the PDF file "Notice of how to...".
