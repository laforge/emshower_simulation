#include "TMath.h"
// Definition of the Longitudinal density
Double_t Longitudinal_density(Double_t a,Double_t b,Double_t t)
{
	return  t>=0?pow(b*t,a-1)*exp(-b*t)/TMath::Gamma(a):0;
}


Double_t Radial_density(Double_t r,Double_t theta,Double_t Rmol) //Rmol is the moliere radius
{
	return /*1/(sqrt(2*TMath::Pi())*Rm*TMath::Pi())*exp(-r*r/(Rm*Rm))*/exp(-TMath::Abs(r))/(2*TMath::Pi());
}


double_t Integrand(Double_t R1,Double_t eta1, Double_t f1, Double_t Rm, Double_t em, Double_t fm, Double_t a, Double_t b, Double_t Rmol)
{Double_t t1;
t1=2*atan(exp(-eta1));
double_t result;
result= TMath::Abs((2*b*exp(b*(R1 - Rm*(cos(f1 - fm)*sech(em)*sin(t1) +
cos(t1)*tanh(em))) - sqrt(pow(Rm,2)*
          (pow(sech(em),2)*pow(sin(f1 - fm),2)*pow(sin(t1),2) +
pow(cos(f1 - fm)*cos(t1)*sech(em) -
sin(t1)*tanh(em),2)))/Rmol)*sin(t1)*
     sqrt(pow(Rm,2)*pow(sech(em),2)*pow(sin(t1),2)*(pow(cos(f1 -
fm),2)*pow(cotan(t1),2) + pow(sin(f1 - fm),2) - 2*cos(f1 -
fm)*cotan(t1)*sinh(em) + pow(sinh(em),2)))*
     pow(b*(-R1 + Rm*cos(f1 - fm)*sech(em)*sin(t1) +
Rm*cos(t1)*tanh(em)),-1 + a))/(TMath::Pi()*TMath::Gamma(a)*(2*pow(cosh(em),2) +
cos(2*t1)*(1 + 2*cos(2*(f1 - fm)) - cosh(2*em)) - 4*cos(f1 -
fm)*sin(2*t1)*sinh(em))));
if(zcyl(R1,eta1,f1,Rm,em,fm)>0){
if(result==result){
return result;}else{return 0;}}
else{return 0;}
}



double_t Longitudinal_Energy(double_t R1,double_t e1,double_t t1,double_t Rm,double_t a, double_t b, double_t Rmol)
{
TF2* f1= new TF2("Intégrante","Integrand([0],[1],[2],[3],x,y,[4],[5],[6])",e1-2,e1+2,t1-1,t1+1);
f1 -> SetParameter(0,R1);
f1 -> SetParameter(1,e1);
f1 -> SetParameter(2,t1);
f1 -> SetParameter(3,Rm);
f1 -> SetParameter(4,a);
f1 -> SetParameter(5,b);
f1 -> SetParameter(6,Rmol);
f1 -> Draw("surf1");
return (f1 -> Integral(e1-1,e1+1,t1-0.2,t1+0.2,1e-5));//+(f1 -> Integral(e1-1,e1-0.0000000001,t1-0.2,t1+0.2,1e-5));
}



double_t Normalisation(Double_t R1,Double_t e1,Double_t f1, Double_t a, Double_t b, Double_t Rmol)
{double_t res; res=0;double_t Rmob;
for(int i=0; i<300;i++){Rmob=R1+i*0.01;res+=Longitudinal_Energy(R1,e1,f1,Rmob,a,b,Rmol);}
return res;
}



double_t Cell_energy(double_t R1,double_t e1,double_t f1,double_t a, double_t b, double_t Rmol,double_t Rmmin,double_t emmin, double_t fmmin, double_t Rmmax, double_t emmax, double_t fmmax)
{double_t res;res=0;double_t R_int;
gROOT->Reset();
TF2* function= new TF2("Intégrante","Integrand([0],[1],[2],[3],x,y,[4],[5],[6])",e1-1,e1+1,f1-0.3,f1+0.3);
function -> SetParameter(0,R1);
function -> SetParameter(1,e1);
function -> SetParameter(2,f1);
function -> SetParameter(3,Rmmin+0.0001);
function -> SetParameter(4,a);
function -> SetParameter(5,b);
function -> SetParameter(6,Rmol);
//function -> Draw("surf1");
for(int i=0; i<int((Rmmax-Rmmin)/0.01) ;i++){R_int=Rmmin+0.01*i;function -> SetParameter(3,R_int);
if(e1<emmax & e1>emmin){res+=((function -> Integral(e1+0.00000001,emmax,fmmin,fmmax))+(function -> Integral(emmin,e1-0.0000001,fmmin,fmmax)))*0.01;}
else{res+=function -> Integral(emmin,emmax,fmmin,fmmax)*0.01;}}
return res;//Normalisation(R1,e1,f1,a,b,Rmol);
}
